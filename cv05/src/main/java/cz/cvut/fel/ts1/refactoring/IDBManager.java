package cz.cvut.fel.ts1.refactoring;

public interface IDBManager {

    public Mail findMail(int mailId);

    public void saveMail(Mail mail);
}
