package cz.cvut.fel.ts1.refactoring;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class MailHelperWithMockitoTest {

    @Test
    public void createAndSendMail_ValidParams_MailStored(){

        // ARRANGE
        IDBManager mockDBManager = mock(IDBManager.class); // mock
        String to = "mkrtcave@fel.cvut.cz";
        String subject = "subject";
        String body = "body";

        Mail mailToReturn = new Mail();
        mailToReturn.setMailId(555);
        mailToReturn.setTo(to);
        mailToReturn.setSubject(subject);
        mailToReturn.setBody(body);
        mailToReturn.setIsSent(false);

        when(mockDBManager.findMail(anyInt())).thenReturn(mailToReturn); // stub
        MailHelper mailHelper = new MailHelper(mockDBManager);

        // ACT
        mailHelper.createAndSendMail(to, subject, body);
        try {
            mailHelper.thread.join(); // block until sending thread is finished
        } catch (InterruptedException ex) {
            fail(ex.getMessage());
        }

        // ASSERT
        verify(mockDBManager,times(2)).saveMail(any(Mail.class));
    }

}
