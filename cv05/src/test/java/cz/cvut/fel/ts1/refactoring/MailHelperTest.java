package cz.cvut.fel.ts1.refactoring;

import org.junit.jupiter.api.Test;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MailHelperTest {

    @Test
    public void createAndSendMail_ValidEmailParametersPassed_EmailSavedToDatabase()
    {
        // ARRANGE
        TestableMailHelper helper = new TestableMailHelper();
        String to = "to@fel.cvut.cz";
        String subject = "Mail subject";
        String body = "body";
        Mail expectedMail = new Mail();
        expectedMail.setTo(to);
        expectedMail.setSubject(subject);
        expectedMail.setBody(body);
        expectedMail.setIsSent(true); // should be updated in storage

        // ACT
        helper.createAndSendMail(to, subject, body);

        // ASSERT
        assertTrue(helper.mailWasStored(expectedMail));
    }

    @Test
    public void createAndSendMail_ValidEmailParametersPassed_SendMailCalledExactlyOnce()
    {
        // ARRANGE
        TestableMailHelper helper = new TestableMailHelper();
        String to = "mkrtcave@fel.cvut.cz";
        String subject = "Mail subject";
        String body = "body";
        int expectedSendMailCallCount = 1;

        // ACT
        helper.createAndSendMail(to, subject, body);

        // ASSERT
        assertEquals(expectedSendMailCallCount, helper.getSendMailCallCounter());
    }

    class TestableMailHelper extends MailHelper {
        private final List<Mail> mailStorage = new ArrayList<Mail>();
        private int sendMailCallCounter = 0;

        public TestableMailHelper() {
            super(new DBManager());
        }

        @Override
        protected void insertMailToDb(Mail mail) {
            mailStorage.add(mail);
        }

        @Override
        protected void sendMailAsync(int mailId) {
            sendMail(mailId); // run the method synchronously
        }

        @Override
        protected Mail loadMail(int mailID) {
            for(Mail m : mailStorage) {
                if (m.getMailId() == mailID) {
                    return m;
                }
            }
            return null;
        }

        @Override
        protected void saveMailChanges(Mail mail) {
            Mail oldMail = loadMail(mail.getMailId());
            if (oldMail != null)
            {
                mailStorage.remove(mail);
            }
            mailStorage.add(mail);
        }

        @Override
        protected void sendMail(MimeMessage message) throws MessagingException {
            sendMailCallCounter++;
        }

        public boolean mailWasStored(Mail mail) {
            return mailStorage.contains(mail);
        }

        public int getSendMailCallCounter() {
            return sendMailCallCounter;
        }
    }
}
